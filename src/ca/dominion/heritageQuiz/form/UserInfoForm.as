package ca.dominion.heritageQuiz.form
{
	import ca.dominion.heritageQuiz.globals.Config;
	
	import caurina.transitions.Tweener;
	
	import flash.events.*;
	import flash.net.*;
	import flash.text.TextField;

	public class UserInfoForm extends UserForm
	{
		public static var USER_EMAIL:String;
		private var _name:InteractiveFormField;
		private var _email:InteractiveFormField;
		private var _postalCode:InteractiveFormField;
		private var _city:InteractiveFormField;
		private var _errorText:TextField;
		private var _submit:SubmitButton;
		
		
		public function UserInfoForm()
		{	
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			alpha = 0;
			
			_name = new InteractiveFormField(nameFld, nameBG, 0, Config.COPY.name.@label);
			_email = new InteractiveFormField(emailFld, emailBG, 1, Config.COPY.email.@label);
			_postalCode = new InteractiveFormField(postalFld, postalBG, 2, Config.COPY.postal.@label);
			_city = new InteractiveFormField(cityFld, cityBG, 3, Config.COPY.city.@label);
			_submit = new SubmitButton(submitBtn, 4, testForm, stage, Config.COPY.submit.toString());
			_errorText = errorTxt;
			
			x += 450;
			Tweener.addTween(this, {alpha:1, transition:'linear', time:1});
			Tweener.addTween(this, {x:x-450, time:1});
			
			
			_name.view.restrict = "A-Z a-z";
			_email.view.restrict = "A-Z a-z @ \\- _ . 0-9";
			_postalCode.view.restrict = "A-Z a-z 0-9";
			_city.view.restrict = "A-Z a-z";
		}
		
		private function testForm(e:MouseEvent):void
		{
			_errorText.text = "";

            if(!_name.isValid)
			{
				_name.selectAll(stage);
				Tweener.addTween(_errorText, {_text:Config.COPY.name.@error.toString(), time:0.3, transition:"linear"});
			}
			else if(!_email.isValidEmail)
			{
				_email.selectAll(stage);
				Tweener.addTween(_errorText, {_text:Config.COPY.email.@error.toString(), time:0.3, transition:"linear"});
			}
			else if(!_postalCode.isValidPostalCode)
			{
				_postalCode.selectAll(stage);
				Tweener.addTween(_errorText, {_text:Config.COPY.postal.@error.toString(), time:0.3, transition:"linear"});
			}
			else
			{
				submitForm();
			}
		}
		
		private function submitForm():void
		{
			var loader:URLLoader = new URLLoader();
			var request:URLRequest = new URLRequest(Config.COPY.submitEmailScript);
			request.method = URLRequestMethod.POST;
			
            var variables:URLVariables = new URLVariables();
            variables.name = _name.text;
            variables.email = USER_EMAIL = _email.text;
            variables.postal = _postalCode.text;
            variables.city = _city.text;
            request.data = variables;
            
            loader.addEventListener(Event.COMPLETE, onLoadComplete);
            loader.addEventListener(ErrorEvent.ERROR, onError);
            //loader.load(request);
            Tweener.addTween(this, {alpha:0, time:0.4, transition:"linear", onComplete:dispatchFormFinished});
		}
		
		private function onError(e:ErrorEvent):void
		{
			
		}
		
		private function onLoadComplete(e:Event):void
		{
			/* var loader:URLLoader = URLLoader(e.target);
			loader.dataFormat = URLLoaderDataFormat.VARIABLES;
			var vars:URLVariables = new URLVariables(loader.data); */
		}
		
		private function dispatchFormFinished():void
		{
			parent.removeChild(this);
			dispatchEvent(new Event(Event.COMPLETE));
		}
		
	}
}