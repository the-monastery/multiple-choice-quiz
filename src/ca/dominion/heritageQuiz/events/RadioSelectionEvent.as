package ca.dominion.heritageQuiz.events {
	
	import flash.events.Event;

	public class RadioSelectionEvent extends Event {
		
		
		
		public static const RADIO_SELECTION:String = "radioSelection";
		
		private var _node:int;
		private var _isSelected:Boolean;
		
		
		
		public function get node():int {
			
			return  _node;
			
		}
		
		
		
		public function get isSelected():Boolean {
			
			return _isSelected;
			
		}
		
		
		
		public function RadioSelectionEvent( type:String, node:int, isSelected:Boolean, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			_node = node;
			_isSelected = isSelected;
			super( type, bubbles, cancelable );
			
		}
		
		
		
		override public function clone():Event {
			
			return new RadioSelectionEvent( type, node, isSelected, bubbles, cancelable );
			
		}
		
		
		
	}
}