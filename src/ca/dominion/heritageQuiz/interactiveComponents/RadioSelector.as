package ca.dominion.heritageQuiz.interactiveComponents
{
	import ca.dominion.heritageQuiz.events.RadioSelectionEvent;
	import ca.dominion.heritageQuiz.globals.Config;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	[Event(name="radioSelection", type="ca.dominion.heritageQuiz.events.RadioSelectionEvent")]
	
	public class RadioSelector extends Sprite
	{
		private var _isChecked:Boolean;
		private var _node:int;
		
		public function RadioSelector(node:int)
		{
			_node = node;
			this.filters = [Config.DROP_SHADOW];
			enable();
		}
		
		public function enable():void
		{
			draw();
			_isChecked = false;
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		public function disable():void
		{
			buttonMode = false;
			removeEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		private function onMouseClick(e:MouseEvent):void
		{
			draw(true);
			_isChecked = true;
			disable();
			dispatchEvent(new RadioSelectionEvent(RadioSelectionEvent.RADIO_SELECTION, _node, true));
		}
		
		private function draw(selected:Boolean = false):void
		{
			graphics.clear();
			graphics.lineStyle(1,0x686868);
			graphics.beginFill(0xFFFFFF);
			graphics.drawCircle(0,0,7.5);
			if(selected)
			{
				graphics.beginFill(0x686868);
				graphics.drawCircle(0,0,4.5);
			}
			graphics.endFill();
		}

	}
}