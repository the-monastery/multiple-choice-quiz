package ca.dominion.heritageQuiz.interactiveComponents
{
	import ca.dominion.heritageQuiz.assets.ButtonBG;
	import ca.dominion.heritageQuiz.globals.*;
	import ca.dominion.heritageQuiz.text.EmbeddedText;
	
	import caurina.transitions.Tweener;
	
	import flash.display.*;
	import flash.events.*;
	
	public class NavigationButton extends Sprite
	{
		private var _label:EmbeddedText;
		private var _textBitmap:Bitmap;
		private var _currentEvent:String;
		
		public function NavigationButton()
		{
			_label = new EmbeddedText("Calibri", 14, 0xFFFFFF, true);
			_textBitmap = new Bitmap();
			
			_label.format.bold = true;
			_label.setFormat();
			
			_textBitmap.x = 10;
			_textBitmap.y = 1;
			
			addChild(new Bitmap(new ButtonBG(0,0)));
			addChild(_textBitmap);
			mouseChildren = false;
			this.alpha = 0;
		}
		
		public function enable(event:String):void
		{
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onMouseClick);
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			Tweener.addTween(this, {alpha:1, x:this.x - 50, time:0.5});
			
			_label.text = getLabel(event);
			createBitmapText();
			
			_currentEvent = event;
		}
		
		public function disable():void
		{
			buttonMode = false;
			removeEventListener(MouseEvent.CLICK, onMouseClick);
			removeEventListener(MouseEvent.ROLL_OVER, onRollOver);
			removeEventListener(MouseEvent.ROLL_OUT, onRollOut);
			Tweener.addTween(this, {alpha:0, x:this.x + 50, time:0.5});
		}
		
		
		private function getLabel(type:String):String
		{
			var label:String;
			
			switch(type)
			{
				case InteractiveEvent.SUBMIT_CHOICE: label = Config.COPY.submit.toString();		break;
				case InteractiveEvent.NEXT_QUESTION: label = Config.COPY.next.toString();		break;
				case InteractiveEvent.GET_RESULTS: 	 label = Config.COPY.results.toString();	break;
				case InteractiveEvent.RESET_QUIZ: 	 label = Config.COPY.resetQuiz.toString();		break;
			}
			
			return label;
		}
		
		private function createBitmapText():void
		{
			_textBitmap.bitmapData = null;
			_textBitmap.bitmapData = new BitmapData(_label.width, _label.height, true, 0);
			_textBitmap.bitmapData.draw(_label);
		}
		
		private function onMouseClick(e:MouseEvent):void
		{
			dispatchEvent(new Event(_currentEvent));
		}
		
		private function onRollOut(e:MouseEvent):void
		{
			
		}
		
		private function onRollOver(e:MouseEvent):void
		{
			
		}

	}
}