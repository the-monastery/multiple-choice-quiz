package ca.dominion.heritageQuiz.globals
{
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;
	
	public class Config
	{
		public static var PROJECT_XML:XML;
		public static var COPY:XML;
		
		public static const CONFIG_URL:String = "xml/config.xml"; 
		
		public static const IMAGE_WIDTH:Number = 200;
		public static const IMAGE_HEIGHT:Number = 270;
		
		public static const DROP_SHADOW:DropShadowFilter = new DropShadowFilter(5,45,0x454545,0.5,5,5,1,2);
		public static const FORM_GLOW:GlowFilter = new GlowFilter(0xE2D097, 0.7, 10, 10, 2, 4);
		 
	}
}