package ca.dominion.heritageQuiz.globals
{
	public class InteractiveEvent
	{
		public static const SUBMIT_CHOICE:String = "submitChoice";
		public static const NEXT_QUESTION:String = "nextQuestion";
		public static const GET_RESULTS:String = "getResults";
		public static const RESET_QUIZ:String = "resetQuiz";
		
		public static const BUILD_OUT_COMPLETE:String = "buildOutComplete";

	}
}