package ca.dominion.heritageQuiz.globals
{
	public class ButtonType
	{
		public static var SUBMIT:String = "Submit";
		public static var NEXT_QUESTION:String = "nextQuestion";
		public static var CHECK_RESULTS:String = "checkResults";
	}
}