package ca.dominion.heritageQuiz.utils
{
	import flash.text.*t;
	
	public class CSSText
	{
		private var _format:TextFormat;
		
		public function get format(font:String, size:Number, color:Number, singleLineAuto:Boolean = false, align:String = TextFormatAlign.LEFT):TextFormat
		{
			return _format;
		}
		
		public function set format(format:TextFormat):void
		{
		 	defaultTextFormat = format;	
		}
		
		public function CSSText()
		{
			_format = new TextFormat(font, size, color);
			_format.align = align;
			
			defaultTextFormat = _format;
			
			selectable = false;
			mouseEnabled = false;
			embedFonts = true;
			gridFitType = GridFitType.PIXEL;
			
			if(singleLineAuto)
			{
				setSingleAuto();
			}
			else
			{
				defaultFormat();
			}
		}
		
		private function defaultFormat():void
		{
			autoSize = TextFieldAutoSize.LEFT;
			multiline = true;
			wordWrap = true;
		}
		
		private function setSingleAuto():void
		{
			width = 0;
			multiline = false;
			wordWrap = false;
			autoSize = TextFieldAutoSize.LEFT;
		}

	}
}