package ca.dominion.heritageQuiz.quiz.components
{
	import ca.dominion.heritageQuiz.events.RadioSelectionEvent;
	import ca.dominion.heritageQuiz.globals.InteractiveEvent;
	
	import caurina.transitions.Tweener;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	public class MultipleChoiceView extends Sprite
	{
		private var _multipleChoices:Array;
		private var _tracker:int;
		private var _currentChoiceList:XMLList;
		
		private var _height:Number;
		
		override public function get height():Number
		{
			return _height;
		}
		
		public function MultipleChoiceView()
		{
			
		}
		
		public function init(list1:XMLList):void
		{
			_multipleChoices = [];
			for(var i:int; i<list1.length();i++)
			{
				var choice:Choice = new Choice(i);
				_multipleChoices.push(choice);
				addChild(choice);
				choice.addEventListener(RadioSelectionEvent.RADIO_SELECTION, onRadioSelection);
			}
		}
		
		public function buildOut():void
		{
			for(var i:int = 0; i < _multipleChoices.length; i++)
			{
				Tweener.addTween(_multipleChoices[i], {y:_multipleChoices[i].y - 20, alpha:0, time:0.5, delay:i/10, onComplete:trackBuildOut});
			}
		}
		
		public function setView(choiceList:XMLList):void
		{
			_tracker = 0;
			_currentChoiceList = choiceList;
			checkNumberOfChoices();
		}
		
		private function trackBuildOut():void
		{
			_tracker++;
			if(_tracker >= _multipleChoices.length)
			{
				dispatchEvent(new Event(InteractiveEvent.BUILD_OUT_COMPLETE));
			}
		}
		
		private function checkNumberOfChoices():void
		{
			var currentLength:int = _currentChoiceList.length();
			var oldLength:int = _multipleChoices.length;
			 
			if(currentLength > oldLength)
			{
				addNewChoices(currentLength - oldLength);	
			}
			else if(currentLength < oldLength)
			{
				removeOldChoices(oldLength - currentLength);
			}
			else
			{
				setChoices();
			}
		}
		
		private function addNewChoices(amount:int):void
		{
			for(var i:int = 0; i < amount; i++)
			{	
				var choice:Choice =  new Choice(_multipleChoices.length + i);			
				_multipleChoices[_multipleChoices.length + i] = choice;
				choice.addEventListener(RadioSelectionEvent.RADIO_SELECTION, onRadioSelection);
			}
			setChoices();
		}
		
		private function removeOldChoices(amount:int):void
		{
			for(var i:int = 0; i < amount; i++)
			{
				Choice(_multipleChoices[_multipleChoices.length - 1]).removeEventListener(RadioSelectionEvent.RADIO_SELECTION, onRadioSelection);
				_multipleChoices.pop();
			}
			setChoices();
		}
		
		private function setChoices():void
		{
			var posY:Number = 0  
			_height = 0;
			
			for(var i:int = 0; i < _currentChoiceList.length(); i++)
			{
				var choice:Choice = Choice(_multipleChoices[i]); 
				choice.setSelectors(_currentChoiceList[i]);
				choice.y = posY;
				posY += choice.height + 5; 
				_height += choice.height +5;
			}
			
			buildIn();
		}
		
		private function buildIn():void
		{
			for(var i:int = 0; i < _multipleChoices.length; i++)
			{
				_multipleChoices[i].alpha = 0;
				Tweener.addTween(_multipleChoices[i], {y:_multipleChoices[i].y + 20, alpha:1, time:0.5, delay:i/10});
			}
		}
		
		private function onRadioSelection(e:RadioSelectionEvent):void
		{
			dispatchEvent(e);
			for(var i:int = 0; i < _multipleChoices.length; i++)
			{
				if(i != e.node)
				{
					Choice(_multipleChoices[i]).enable();
				}
			}
		}

	}
}