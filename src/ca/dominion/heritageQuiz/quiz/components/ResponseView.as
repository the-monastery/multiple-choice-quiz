package ca.dominion.heritageQuiz.quiz.components
{
	import ca.dominion.heritageQuiz.globals.InteractiveEvent;
	import ca.dominion.heritageQuiz.text.EmbeddedText;
	
	import caurina.transitions.Tweener;
	
	import flash.display.*;
	import flash.events.Event;
	
	public class ResponseView extends Sprite
	{
		private var _responseTextFld:EmbeddedText;
		private var _textBitmap:Bitmap;
		
		public function ResponseView()
		{
			mouseEnabled = false;
			mouseChildren = false;
			_responseTextFld = new EmbeddedText("Calibri", 14, 0x333333);
			_textBitmap =  new Bitmap();
			addChild(_textBitmap);
			_responseTextFld.width = 335;
			alpha = 0;
		}
		
		public function setRespose(text:String):void
		{
			_responseTextFld.htmlText = text;
			createBitmapText();
		}
		
		public function buildOut():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, onComplete:dispatchEvent, onCompleteParams:[new Event(InteractiveEvent.BUILD_OUT_COMPLETE)]});
		}
		
		private function createBitmapText():void
		{
			_textBitmap.bitmapData = null;
			_textBitmap.bitmapData = new BitmapData(335, _responseTextFld.height, true, 0);
			_textBitmap.bitmapData.draw(_responseTextFld);
			Tweener.addTween(this, {alpha:1, time:0.5, delay:0.3});
		}

	}
}