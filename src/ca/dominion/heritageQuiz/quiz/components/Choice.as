package ca.dominion.heritageQuiz.quiz.components
{
	import ca.dominion.heritageQuiz.events.RadioSelectionEvent;
	import ca.dominion.heritageQuiz.globals.Config;
	import ca.dominion.heritageQuiz.interactiveComponents.RadioSelector;
	import ca.dominion.heritageQuiz.text.EmbeddedText;
	
	import flash.display.*;
	
	public class Choice extends Sprite
	{
		private var _radioBtn:RadioSelector;
		private var _text:EmbeddedText;
		private var _bitmapText:Bitmap;
		
		public function Choice(node:int)
		{
			_radioBtn = new RadioSelector(node);
			_bitmapText = new Bitmap();
			_text = new EmbeddedText("Calibri", Config.PROJECT_XML.mainText.@size, Config.PROJECT_XML.mainText.@color); 
			
			_text.width = 280;
			
			_bitmapText.x = _radioBtn.width + 15;
			_radioBtn.x = _radioBtn.width/2;
			
			_radioBtn.addEventListener(RadioSelectionEvent.RADIO_SELECTION, onRadioSelection);
			
			addChild(_radioBtn);
			addChild(_bitmapText);
		}
		
		public function setSelectors(text:String):void
		{
			_radioBtn.enable();
			_text.text = text;
			positionRadioBtn();
			createBitmapTextField();
		}
		
		public function enable():void
		{
			_radioBtn.enable();
		}
		
		private function createBitmapTextField():void
		{
			_bitmapText.bitmapData = null;
			_bitmapText.bitmapData = new BitmapData(280, _text.height, true, 0);
			_bitmapText.bitmapData.draw(_text);
		}
		
		private function positionRadioBtn():void
		{
			_radioBtn.y = _text.height/2;
		}
		
		private function onRadioSelection( e:RadioSelectionEvent ):void
		{
			dispatchEvent(e);
		}

	}
}