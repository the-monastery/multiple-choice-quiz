package ca.dominion.heritageQuiz.quiz.components
{
	import ca.dominion.heritageQuiz.globals.Config;
	import ca.dominion.heritageQuiz.utils.ImageLoader;
	
	import caurina.transitions.*;
	
	import flash.display.Sprite;
	
	public class AnimatedImage extends Sprite
	{
		private var _imageLoader:ImageLoader;
		private var _currentURL:String;
		private var _width:Number;
		private var _height:Number;
		
		override public function get width():Number
		{
			return _width;
		}
		
		override public function get height():Number
		{
			return _height;
		}
		
		public function AnimatedImage(width:Number, height:Number)
		{
			_imageLoader = new ImageLoader();
			_width = width;
			_height = height;
			
			_imageLoader.x = -_width/2;
			_imageLoader.y = -_height/2;
			addChild(_imageLoader);
			
			this.filters = [Config.DROP_SHADOW];
		}
		
		public function load(url:String):void
		{
			_currentURL = url;
			Tweener.addTween(this, {scaleX:0, scaleY:0, alpha:0, time:0.3, transition:Equations.easeInBack, onComplete:buildOutComplete});
		}
		
		private function buildOutComplete():void
		{
			_imageLoader.load(_currentURL, buildIn); 
		}
		
		private function buildIn():void
		{
			Tweener.addTween(this, {scaleX:1, scaleY:1, alpha:1, transition:Equations.easeOutBack, time:0.3});
		}
		

	}
}