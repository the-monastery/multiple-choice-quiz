package ca.dominion.heritageQuiz.quiz.data
{
	public class QuestionArray
	{
		private var _questions:Array;
		private var _isLocked:Boolean;
		
		public function QuestionArray()
		{
			_questions = new Array();
			_isLocked = false;
		}
		
		public function push(question:Question):void
		{
			if(!_isLocked)
			{
				_questions.push(question);	
			}
			else
			{
				throw(new Error("QuestionsArray cannot accept new elements after passed into the Quiz"));
			}
		}
		
		public function lock():void
		{
			_isLocked = true;
		}
		
		public function get length():int
		{
			return _questions.length;
		}
		
		public function get array():Array
		{
			return _questions;
		}
	}
}