package ca.dominion.heritageQuiz.quiz.data
{
	public class Question
	{
		public var id:int;
		public var text:String;
		public var options:XMLList;
		public var imageURL:String;
		public var answer:int;
		public var response:String;
		
		public function Question()
		{
		}

		public function toString():String
		{
			var optionsS:String = "";
			
			for each(var item:String in options)
			{
				optionsS += item+"\n";
			}
			
			return "ID: " + id + "\nText: " + text + "\nOptions:\n" + optionsS + "URL: " + imageURL + "\nAnswer: " + options[answer].toString() + "\nResponse: " + response + "\n\n"; 
		}

	}
}