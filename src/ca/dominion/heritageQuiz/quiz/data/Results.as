package ca.dominion.heritageQuiz.quiz.data {

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class Results {



		private var _resultsList:Array;
		private var _numCorrect:int;
		private var _totalQuestions:int;
		private var _currentQuestion:int;
		
		
		
		/**
		 * 
		 * 
		 */
		public function Results() {}
		
		/**
		 * 
		 * @param totalQuestions
		 * 
		 */
		public function init( totalQuestions:int ):void {
		
			_resultsList = [];
			_totalQuestions = totalQuestions;
			_numCorrect = 0;
			_currentQuestion = 0;
			
		}
		
		/**
		 * 
		 * @param question
		 * @param response
		 * @param isCorrect
		 * 
		 */
		public function addResponse( question:int, response:int, isCorrect:Boolean ):void {
		
			_currentQuestion = question;
			_resultsList[ question ] = response;
			isCorrect && _numCorrect++;
			
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get numCorrect():int {
		
			return _numCorrect;	
			
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get runningPercentage():Number {
		
			return _numCorrect / _currentQuestion * 100;
			
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get totalPercentage():Number {
		
			return Math.round( _numCorrect / _totalQuestions * 100 );
			
		}
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function getDataPairs():String {
		
			var dataPairs:String = "";
			
			for( var val:String in _resultsList ) {
				dataPairs += val + '=' + _resultsList[ val ] + "&";
			}
			
			return dataPairs.substr( 0, dataPairs.length - 1 );
			
		}
		
	}
}