package ca.dominion.heritageQuiz.utils
{
	import flash.events.Event;
	import flash.net.*;
	
	public class XMLLoader
	{
		private var _urlLoader:URLLoader;
		private var _callBack:Function;
		
		public function XMLLoader()
		{
			_urlLoader = new URLLoader();
		}
		
		public function load(url:String, callBack:Function):void
		{
			_callBack = callBack;
			_urlLoader.addEventListener(Event.COMPLETE, onComplete);
			_urlLoader.load(new URLRequest(url));	
		}
		
		private function onComplete(e:Event):void
		{
			_urlLoader.removeEventListener(Event.COMPLETE, onComplete);
			_callBack(e);	
		}
	}
}