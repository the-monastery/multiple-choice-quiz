package ca.dominion.heritageQuiz.utils {
	
	import ca.dominion.heritageQuiz.globals.Config;
	
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class ImageCacher extends Loader {
		
		
		
		private var _imgList:Array;
		private var _current:int;
		
		
		
		/**
		 * 
		 * 
		 */
		public function ImageCacher() {
			
		}
		
		
		
		/**
		 * 
		 * @param imgList
		 * 
		 */
		public function loadImageList(imgList:Array):void {
			
			_imgList = [];
			_imgList = imgList;
			_current = 0;
			contentLoaderInfo.addEventListener( Event.COMPLETE, onComplete );
			contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR, onIOError );
			loadNextImage();
			
		}
		
		
		
		private function loadNextImage():void {
			
			load(new URLRequest(_imgList[_current]));
			
		}
		
		
		
		private function onComplete(e:Event):void {
			
			_current++;
			if( _current < _imgList.length ) {
				loadNextImage();
			}
			else {
				contentLoaderInfo.removeEventListener( Event.COMPLETE, onComplete );
			}
			
		}
		
		
		
		private function onIOError( e:IOErrorEvent ):void {
			
			load( new URLRequest( Config.PROJECT_XML.defaultPhoto.@url ) );
			
		}
		
		
		
	}
}