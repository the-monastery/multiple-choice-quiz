package ca.dominion.heritageQuiz.projectComponents
{
	import ca.dominion.heritageQuiz.globals.Config;
	
	import caurina.transitions.Tweener;
	
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix;

	public class Background extends Sprite
	{
		public function Background()
		{
			alpha = 0;
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void
		{
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(stage.stageWidth, stage.stageHeight, -Math.PI/2);
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			graphics.beginGradientFill(	GradientType.LINEAR, 
										[Config.PROJECT_XML.background.@color1, Config.PROJECT_XML.background.@color2],
										[1,1],
										[0,255],
										matrix);
			graphics.drawRect(0,0,stage.stageWidth,stage.stageHeight);
			graphics.endFill();
			
			Tweener.addTween(this, {alpha:1, time:2});
		}
		
	}
}